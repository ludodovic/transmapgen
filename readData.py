import matplotlib.pyplot as plt
import numpy as np
import os

def read_hgt_file(file_path, sizeX = 3601, sizeY = 3601):
    with open(file_path, 'rb') as file:
        data = np.fromfile(file, dtype=np.dtype('>i2'), count=sizeX*sizeY)  # Lire les données en format big endian int16
        data = data.reshape((sizeX, sizeY))  # Remodeler les données en une matrice 2D (3601x3601)
    return data