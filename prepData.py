import numpy as np
import os
import math
import readData

def get_filename_by_pos(x, y): # x & y from 1 to n, not zero
    x_est_or_west = "e"
    if x <=5:
        x_est_or_west = "w"
    x_index = str(abs(x-6))
    y_index = str(y+41)
    return "n" + y_index + x_est_or_west + "00" + x_index + ".hgt"

def get_file_list(start_x, end_x, start_y, end_y):
    filename_list = []
    for x in range(start_x, end_x+1):
        for y in range(start_y, end_y+1):
            filename_list += [get_filename_by_pos(x,y)]
    return filename_list

def concatenate_hgt_images_list(dir_name, file_names, arg_axis=1):
    left_image = readData.read_hgt_file(dir_name + file_names[0] + ".hgt")
    for i in range(1, len(file_names)):
        right_image = readData.read_hgt_file(dir_name + file_names[i] + ".hgt")
        left_image = np.concatenate((left_image, right_image), axis=arg_axis)

    return left_image

def concatenate_hgt_map(dir_name, start_x, end_x, start_y, end_y):
    filename_list = get_file_list(start_x, end_x, start_y, end_y)

    prev_img = readData.read_hgt_file(dir_name + filename_list[0])
    prev_row = readData.read_hgt_file(dir_name + filename_list[0])
    reader_index = 0
    
    for x in range(0, abs(start_x-end_x)+1):
        for y in range(0, abs(start_y-end_y)+1):
            reader_index = x*(abs(start_y-end_y)+1)+y
            if (y==0):
                prev_img = readData.read_hgt_file(dir_name + filename_list[reader_index])
                continue
            next_img = readData.read_hgt_file(dir_name + filename_list[reader_index])
            prev_img = np.concatenate((next_img, prev_img), axis=0)
        if (x==0):
            prev_row = prev_img
            continue
        next_row = prev_img
        prev_row = np.concatenate((prev_row, next_row), axis=1)

    savefile_name = dir_name + "concatenated_x"+str(start_x)+"_"+str(end_x)+"_y"+str(start_y)+"_"+str(end_y)+".hgt"
    with open(savefile_name,'wb') as f:
        prev_row.astype('>i2').tofile(f)
        print("Merging finished")
        print("-Used Files:")
        for filen in filename_list:
            print("--["+filen+"]")
        print("-File info:")
        print("--dim: "+str(prev_row.shape))
        print("saved as: " + savefile_name)

    return prev_row